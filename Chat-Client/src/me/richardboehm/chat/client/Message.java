package me.richardboehm.chat.client;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {
    public int authorId;
    public Timestamp timestamp;
    public int groupId;
    public String content;

    public Message(int id, int group, String message){
        this.authorId = id;
        this.groupId = group;
        this.content = message;
    }


    public Message(String[] input){
        this.authorId = Integer.parseInt(input[0]);
        this.groupId = Integer.parseInt(input[1]);

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            Date parsedDate = null;
            parsedDate = dateFormat.parse(input[2]);
            this.timestamp = new java.sql.Timestamp(parsedDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            this.timestamp = new Timestamp(System.currentTimeMillis());
        }
        this.content = input[3];
    }
}
