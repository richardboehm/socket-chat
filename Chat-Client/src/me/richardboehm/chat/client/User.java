package me.richardboehm.chat.client;

public class User {
    public String username;
    public String password;
    public int id;

    public User(int id, String username){
        this.id = id;
        this.username = username;
    }
}
