package me.richardboehm.chat.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * A multithreaded chat room server.  When a client connects the
 * server requests a screen username by sending the client the
 * text "SUBMITNAME", and keeps requesting a username until
 * a unique one is received.  After a client submits a unique
 * username, the server acknowledges with "NAMEACCEPTED".  Then
 * all messages from that client will be broadcast to all other
 * clients that have submitted a unique screen username.  The
 * broadcast messages are prefixed with "MESSAGE ".
 *
 * Because this is just a teaching example to illustrate a simple
 * chat server, there are a few features that have been left out.
 * Two are very useful and belong in production code:
 *
 *     1. The protocol should be enhanced so that the client can
 *        send clean disconnect messages to the server.
 *
 *     2. The server should do some logging.
 */
public class ChatServer {

    /**
     * The port that the server listens on.
     */
    private static final int PORT = 9001;
    private static final HashMap<Integer, User> users = new HashMap<>(); // HashSet<Integer> users = new HashSet<Integer>();
    private static HashSet<PrintWriter> writers = new HashSet<PrintWriter>();
    private static Connection connection = null;
    private static Properties connectionProps = new Properties();

    private static String username = "chat";
    private static String password = "chat";
    private static String databaseServer = "127.0.0.1";
    private static int databasePort = 3306;
    private static String databaseTable = "chat";

    public static void main(String[] args){
        System.out.printf("The chat server is running on port %d.%n", PORT);

        try (ServerSocket listener = new ServerSocket(PORT)) {

            connectDatabase();
            while (true) {
                new Handler(listener.accept()).start();
            }
        }catch (Exception e){
            System.out.println(e);
        }
    }

    private static void connectDatabase() throws SQLException {
        connectionProps.put("user", username);
        connectionProps.put("password", password);
        connectionProps.put("serverTimezone", "UTC");
        connection = DriverManager.getConnection(
                "jdbc:mysql://" +
                        databaseServer + ":" +
                        databasePort + "/" +
                        databaseTable,
                connectionProps);

        System.out.printf("Connected to database %s. %n", databaseServer);
    }

    /**
     * A handler thread class.  Handlers are spawned from the listening
     * loop and are responsible for a dealing with a single client
     * and broadcasting its messages.
     */
    private static class Handler extends Thread {
        private User user;
        private String username;
        private String userPassword;
        private Socket socket;
        private BufferedReader in;
        private PrintWriter out;
        private PreparedStatement statement;

        /**
         * Constructs a handler thread, squirreling away the socket.
         * All the interesting work is done in the run method.
         */
        public Handler(Socket socket) {
            this.socket = socket;
        }

        /**
         * Services this thread's client by repeatedly requesting a
         * screen username until a unique one has been submitted, then
         * acknowledges the username and registers the output stream for
         * the client in a global set, then repeatedly gets inputs and
         * broadcasts them.
         */
        public void run() {
            try {

                // Create character streams for the socket.
                in = new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);

                // Request a username from this client.  Keep requesting until
                // a username is submitted that is not already used.  Note that
                // checking for the existence of a username and adding the username
                // must be done while locking the set of users.
                while (true) {
                    sendMessage("SUBMITNAME");
                    username = in.readLine();
                    userPassword= in.readLine();

                    if (username == null) {
                        return;
                    }

                    user = authenticateUser(username, userPassword);

                    synchronized (users) {
                        if (!users.containsKey(user.id)) {
                            users.put(user.id, user);
                            break;
                        }
                    }

                }

                // Now that a successful username has been chosen, add the
                // socket's print writer to the set of all writers so
                // this client can receive broadcast messages.
                sendMessage("NAMEACCEPTED");
                writers.add(out);

                String onlineUsers= users.entrySet().stream()
                        .map(x -> x.getKey() + ";" +x.getValue().username)
                        .collect(Collectors.joining(","));

                broadcastMessage("USERS", onlineUsers);

                Message[] messages = fetchMessageHistory(0);
                for (Message message :
                        messages) {
                    if(message == null){
                        continue;
                    }
                    sendMessage("MESSAGE", String.format("%s;%s;%s;%s", message.authorId, message.groupId, message.timestamp, message.content));
                }

                // Accept messages from this client and broadcast them.
                // Ignore other clients that cannot be broadcasted to.
                while (true) {
                    String input = in.readLine();
                    if (input == null) {
                        return;
                    }

                    logMessage(user.id, 0, input);

                    broadcastMessage(user, 0, input);
                }
            } catch (IOException | SQLException e) {
                System.out.println(e);
            } finally {
                // This client is going down!  Remove its username and its print
                // writer from the sets, and close its socket.
                if (user != null) {
                    users.remove(user.id);
                }
                if (out != null) {
                    writers.remove(out);
                }
                try {
                    socket.close();
                } catch (IOException e) {
                }
            }
        }

        private void broadcastMessage(User user, int groupId, String message) {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            broadcastMessage("MESSAGE", String.format("%s;%s;%s;%s", user.id, groupId, timestamp, message));
        }

        private void broadcastMessage(String protocol, String message) {
            for (PrintWriter writer : writers) {
                writer.println(String.format("%s %s", protocol, message));
            }
        }

        private void sendMessage(String protocol, String message){
            out.println(protocol + " " + message);
        }

        private void sendMessage(String protocol){
            out.println(protocol);
        }

        private User authenticateUser(String name, String pass) throws SQLException {
            try {
                statement = connection.prepareStatement("SELECT id, name FROM `USERS` INNER JOIN `PASSWORDS` WHERE `users`.`name` LIKE ? AND `passwords`.`hash` LIKE ?");
                statement.setString(1, name);
                statement.setString(2, pass);
                ResultSet resultSet = statement.executeQuery();
                resultSet.next();
                return new User(resultSet.getInt("id"), resultSet.getString("name"));

            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            } finally {
                if (statement!= null) {
                    statement.close();
                }
            }
        }

        private void logMessage(int id, int groupId, String input) throws SQLException {
            System.out.printf("User: %s \t Group: %s \t Message: %s %n", id, groupId, input);
            try {
                PreparedStatement statement = connection.prepareStatement("INSERT INTO `messages` (`id`, `userId`, `groupId`, `timestamp`, `content`) VALUES (NULL, ?, ?, ?, ?)");
                statement.setInt(1, id);
                statement.setInt(2, groupId);
                statement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
                statement.setString(4, input);

                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (statement!= null) {
                    statement.close();
                }
            }

        }

        private Message[] fetchMessageHistory(int groupId) throws SQLException {
            PreparedStatement statement = null;
            Message[] messages = null;
            try {
                statement = connection.prepareStatement("SELECT * FROM messages WHERE groupId = ? ");
                statement.setInt(1, groupId);
                ResultSet resultSet = statement.executeQuery();

                resultSet.last();
                int count = resultSet.getRow();
                messages = new Message[count];
                resultSet.beforeFirst();
                for(int i = 0; i < count; i++){
                    resultSet.next();
                    messages[i] = new Message(resultSet.getInt("userId"), groupId, resultSet.getTimestamp("timestamp"), resultSet.getString("content"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (statement!= null) {
                    statement.close();
                }
            }
            return messages;
        }

        private HashMap<Integer, User> fetchOnlineUsers(){
            return users;
        }
    }
}