package me.richardboehm.chat.server;

import java.sql.Timestamp;

public class Message {
    public int authorId;
    public int groupId;
    public Timestamp timestamp;
    public String content;

    public Message(int id, int group, Timestamp timestamp, String message){
        this.authorId = id;
        this.groupId = group;
        this.timestamp = timestamp;
        this.content = message;
    }
}
