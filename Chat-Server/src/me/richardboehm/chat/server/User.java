package me.richardboehm.chat.server;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class User {
    public String username;
    public String password;
    public int id;

    public User(int id, String username){
        this.id = id;
        this.username = username;
    }
}
